local srcpath = core.get_modpath("aes_profile") .. "/src"

aes_profile = {}


dofile(srcpath .. "/api.lua")
dofile(srcpath .. "/chat.lua")
dofile(srcpath .. "/commands.lua")
dofile(srcpath .. "/GUI.lua")
dofile(srcpath .. "/items.lua")
dofile(srcpath .. "/player_manager.lua")