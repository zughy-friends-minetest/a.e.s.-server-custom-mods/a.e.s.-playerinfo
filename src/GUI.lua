local S = core.get_translator("aes_profile")
local FS = function(...) return core.formspec_escape(S(...)) end

local function get_formspec() end

local COUNTRY_CODES_TBL = country_lib.get_country_codes()
table.insert(COUNTRY_CODES_TBL, 1, "---")
local PRONOUNS_TBL = {
  "---",
  "He",
  "She",
  "They"
}

local COUNTRY_CODES = table.concat(COUNTRY_CODES_TBL, ",")
local PRONOUNS = "---," .. FS("He") .. "," .. FS("She") .. "," .. FS("They")
local NO_TITLE_TXT = "<" .. FS("unlocked titles") .. ">"



function aes_profile.get_profile_formspec(p_name, t_name)
  local read_only = t_name ~= nil
  t_name = t_name or p_name

  core.show_formspec(p_name, "aes_profile:profile", get_formspec(t_name, read_only))
end





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function get_formspec(p_name, read_only)
  local p_roles = roles.get_equippable_roles(p_name)
  local titles = {}

  for _, role in ipairs(p_roles) do
    titles[#titles+1] = roles.get_def(role).name
  end
  table.insert(titles, 1, NO_TITLE_TXT)
  local titles_fs = table.concat(titles, ",")

  local curr_country = aes_profile.get_country(p_name) or "---"
  local curr_pronoun = aes_profile.get_pronoun(p_name) or "---"
  local curr_title = roles.get_equipped_role(p_name) and roles.get_equipped_role(p_name).name or NO_TITLE_TXT

  local title_head = roles.get_nametag(p_name, false)

  local p_skin = collectible_skins.get_player_skin(p_name) or {model = "character.b3d", texture = "aesprofile_fs_unknown.png"} -- in caso non si sia mai connessə da quando collectible_skins è stata attivata
  local p_skins = collectible_skins.get_player_skins(p_name)
  local p_achs = achvmt_lib.get_player_achievements(p_name)
  local skins_amnt = p_skins and #p_skins or "4?" -- idem
  local ach_amnt = p_achs and #p_achs or 0 -- in caso non si sia mai connessə da quando achievements_lib è stata attivata

  local name, dropdown_fields

  if not read_only then
    local country_idx = table.indexof(COUNTRY_CODES_TBL, curr_country)
    local pronoun_idx = table.indexof(PRONOUNS_TBL, curr_pronoun)
    local title_idx = table.indexof(titles, curr_title)

    name = "hypertext[0.5,0.9;5.5,0.8;pname_txt;<global size=24 halign=center valign=middle><b>" .. p_name .. "</b>]"
    dropdown_fields = {
      "container[0.4,2]",
      "image[-0.06,-0.5;0.5,0.5;aesprofile_fs_questionmark.png]",
      "tooltip[-0.06,-0.5;0.5,0.5;" .. FS("Your Country code, your pronoun and a title.\n\nIf set, they'll all appear next to your nick when you write a message in chat") .. "]",
      "dropdown[0,0;1.2,0.65;country;" .. COUNTRY_CODES .. ";" .. country_idx .. "]",
      "dropdown[1.3,0;1.2,0.65;pronoun;" .. PRONOUNS .. ";" .. pronoun_idx .. ";true]",
      "dropdown[2.6,0;3.1,0.65;title;" .. titles_fs .. ";" .. title_idx .. ";true]",
      "container_end[]",
    }

    dropdown_fields = table.concat(dropdown_fields, "")

  else
    name = {
      "container[0,0.4]",
      "hypertext[0.5,0.9;5.5,0.8;pname_txt;<global size=24 halign=center valign=middle><b>" .. p_name .. "</b>]",
      "hypertext[2.9,1.4;5.5,0.8;country;<global size=12 valign=middle>     | " .. FS(curr_pronoun) .. "]",
      "image[2.8,1.62;0.35,0.26;" .. country_lib.get_country_flag(curr_country) .. "]",
      "tooltip[2.8,1.62;0.35,0.26;" .. country_lib.get_country_name(curr_country) .. "]",
      "container_end[]"
    }

    name = table.concat(name, "")
    dropdown_fields = ""
  end

  local formspec = {
    "formspec_version[6]",
    "size[6.5,8.2]",
    "no_prepend[]",
    "background[0,0;6.5,8.2;aesprofile_bg.png]",
    "bgcolor[;true]",
    "hypertext[0.5,-0.08;5.5,0.8;pname_txt;<global size=18 halign=center valign=middle><b>" .. FS("Player profile") .. "</b>]",
    name,

    dropdown_fields,

    "container[0,0.4]",
    "image[0.4,2.45;5.7,5;aesprofile_fs_bgplayer.png^[opacity:135]",
    "hypertext[0.5,2.6;5.5,0.8;pname_txt;<global size=16 halign=center valign=middle>" .. core.formspec_escape(title_head) .. "]",
    "model[0,3.3;6.5,3.6;chara;" .. p_skin.model .. ";" .. p_skin.texture .. ";0,-150;false;true]",
    "container[0.1,-1.8]",
    "image[0.5,6.3;0.5,0.5;achievements_lib_item.png]",
    "image[0.5,6.9;0.5,0.5;cskins_wardrobe.png]",
    "tooltip[0.5,6.3;0.5,0.5;" .. FS("Unlocked achievements") .. "]",
    "tooltip[0.5,6.9;0.5,0.5;" .. FS("Unlocked skins") .. "]",
    "hypertext[1.1,6.4;1,0.8;ach_amnt;<global size=14 halign=left>" .. ach_amnt .. "]",
    "hypertext[1.1,7.03;1,0.8;skinks_amnt;<global size=14 halign=left>" .. skins_amnt .. "]",
    "container_end[]",
    "container_end[]",
    "image_button_exit[6.8,0.85;0.5,0.5;arenalib_infobox_quit.png;quit;;true;false;]"
  }

  return table.concat(formspec, "")
end





----------------------------------------------
---------------GESTIONE CAMPI-----------------
----------------------------------------------

core.register_on_player_receive_fields(function(player, formname, fields)
	if formname ~= "aes_profile:profile" then return end

  local p_name = player:get_player_name()

  -- li lancia tutti insieme se premo su elementi che non siano i menù a tendina;
  -- da qua il controllo
  if fields.country and not fields.pronoun then
    local code = string.len(fields.country) == 2 and fields.country or nil -- sennò è `---` o cose mandate a caso tipo `43`
    aes_profile.set_country(p_name, code)

  elseif fields.pronoun and not fields.country then
    local pronoun

    if fields.pronoun ~= "1" then
      local pronoun_idx = tonumber(fields.pronoun)
      if not pronoun_idx or pronoun_idx <= 0 or pronoun_idx > #PRONOUNS_TBL then return end -- sanity checks

      pronoun = fields.pronoun ~= "1" and PRONOUNS_TBL[pronoun_idx]
    end

    aes_profile.set_pronoun(p_name, pronoun)

  elseif fields.title and not fields.country then
    if fields.title ~= "1" then
      local p_roles = roles.get_equippable_roles(p_name)

      if not tonumber(fields.title) then return end -- sanity check pt.1

      local role_id = tonumber(fields.title) -1

      if role_id > 0 and role_id <= #p_roles then -- sanity check pt.2
        roles.set_equipped_role(p_name, p_roles[role_id])
      end
    else
      roles.clear_equipped_role(p_name)
    end

    core.show_formspec(p_name, "aes_profile:profile", get_formspec(p_name)) -- così aggiorna la scritta sul modello
  end
end)