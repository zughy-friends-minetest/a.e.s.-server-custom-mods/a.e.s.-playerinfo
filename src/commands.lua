local S = core.get_translator("aes_profile")



core.register_chatcommand("profile", {
  description = S("Show either your profile or the one belonging to the specified player"),
  params = "[" .. S("player") .. "]",
  func = function(name, param)
    -- se è in arena, annullo
    if arena_lib.is_player_in_arena(name) then return false end

    local params = param:split(" ")

    if params[1] then
      if not core.get_auth_handler().get_auth(params[1]) then
        core.sound_play("cskins_deny", {to_player = name})
        core.chat_send_player(name, core.colorize("#e6482e", S("Unknown player!")))
        return end

      aes_profile.init_player(params[1])
    end

    aes_profile.get_profile_formspec(name, params[1])
    return true
  end
})