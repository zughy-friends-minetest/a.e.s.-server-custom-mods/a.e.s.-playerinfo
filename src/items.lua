local S = core.get_translator("aes_profile")



core.register_tool("aes_profile:profile", {
  description = S("Profile"),
  inventory_image = "aesprofile_item.png",
  groups = {not_in_creative_inventory = 1, oddly_breakable_by_hand = 3},
  on_place = function() end,
  on_drop = function() end,

  on_use = function(itemstack, user, pointed_thing)
    aes_profile.get_profile_formspec(user:get_player_name())
  end
})
