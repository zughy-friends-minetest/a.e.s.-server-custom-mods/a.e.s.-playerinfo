local S = core.get_translator("aes_profile")
local format_chat_message = core.format_chat_message



function core.format_chat_message(p_name, msg)
  local country_str = aes_profile.get_country(p_name)
  local pronoun_str = aes_profile.get_pronoun(p_name)
  local country = country_str and (country_str) or ""
  local pronoun = pronoun_str and (S(pronoun_str) .. " ") or ""

  if pronoun_str and country_str then
    country = country .. ", "
  elseif country_str then
    country = country .. " "
  end

  local profile_roles_separator = ""
  if (pronoun_str or country_str) and roles.get_nametag(p_name, false):sub(1,1) == "[" then
    profile_roles_separator = core.colorize("#cfc6b8", "| ")
  end

  if arena_lib.is_player_in_arena(p_name) then
    return format_chat_message(p_name, msg)
  else
    return core.colorize("#eebbbb", country) .. core.colorize("#eebbbb", pronoun) .. profile_roles_separator .. format_chat_message(p_name, msg)
  end
end
