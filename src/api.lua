local storage = core.get_mod_storage()
local data = {}



function aes_profile.init_player(p_name)
  if data[p_name] then return end

  local p_storage = storage:get_string(p_name)

  if p_storage == "" then
    data[p_name] = {}
  else
    data[p_name] = core.deserialize(p_storage)
  end
end



function aes_profile.get_country(p_name)
  return data[p_name].country
end



function aes_profile.get_pronoun(p_name)
  return data[p_name].pronoun
end



function aes_profile.set_country(p_name, country)
  data[p_name].country = country
  storage:set_string(p_name, core.serialize(data[p_name]))
end



function aes_profile.set_pronoun(p_name, pronoun)
  data[p_name].pronoun = pronoun
  storage:set_string(p_name, core.serialize(data[p_name]))
end